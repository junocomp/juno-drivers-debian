#!/bin/bash
lsb_dist_id="$(lsb_release -si)"  
product_model="$(sed -e 's/^\s*//g' -e 's/\s*$//g' "/sys/devices/virtual/dmi/id/product_name" | tr ' ,/-' '_')"
chassis="$(hostnamectl | awk 'FNR==3 {print $2}')"
codename="$(lsb_release -c | awk '{print $2}')"
de="$(ps axf | grep -Po "env \.*[A-Z]+" | awk '{print $2}' | sort -u)"
ecsys="$(cat /boot/config-$(uname -r) | grep CONFIG_ACPI_EC)"

#--------------------------------------

update-pciids

#Install Flathub
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

#Add User to Audio
echo "Adding $USER to Input"
sed 's/#EXTRA_GROUPS.*/EXTRA_GROUPS="input clevo-keyboard adm"/' -i /etc/adduser.conf
sed 's/#ADD_EXTRA_GROUPS=1/ADD_EXTRA_GROUPS=1/g' -i /etc/adduser.conf

#GRUB
echo "Updating Grub"
sleep 3

case "$product_model" in
    NUC10i3FNH|NUC10i5FNH|NUC10i7FNH|NUC10i3FNK|NUC10i5FNK|NUC10i7FNK)
    echo "Olympia 8th Gen ($chassis)"
    if 
	grep 'hw:0,0' /etc/pulse/default.pa
	then 
	echo "Olympia (10th Gen) headphone jack drivers already exist";
	else
	echo 'load-module module-alsa-sink device=hw:0,0' >> /etc/pulse/default.pa;
	echo "Olympia (10th Gen) headphone jack drivers installed";
    fi
    ;;
esac

sed -i -e '/GRUB_CMDLINE_LINUX_DEFAULT/d' /etc/default/grub;
sed -i '/\GRUB_DISTRIBUTOR/a GRUB_CMDLINE_LINUX_DEFAULT="quiet splash $(/usr/share/junocomp/juno-grub) fsck.mode=force fsck.repair=yes"' /etc/default/grub
update-grub
update-initramfs -u -k all

echo "GRUB has been updated"

# Enable Systemd
systemctl enable thermald

# Disable Power Profiles Rules for Desktops
case "$product_model" in
    NUC10i3FNH|NUC10i5FNH|NUC10i7FNH|NUC10i3FNK|NUC10i5FNK|NUC10i7FNK|To_Be_Filled_By_O.E.M.|BQM5|TB_4000|SYSTEM_PRODUCT_NAME)
    if [ -f /etc/udev/rules.d/power-profiles.rules ]; then
    rm /etc/udev/rules.d/power-profiles.rules;
    fi
    ;;
esac

# Enable Juno PP
case "$product_model" in
    NUC10i3FNH|NUC10i5FNH|NUC10i7FNH|NUC10i3FNK|NUC10i5FNK|NUC10i7FNK|To_Be_Filled_By_O.E.M.|BQM5|TB_4000|SYSTEM_PRODUCT_NAME)
    systemctl disable juno-pp.service
    ;;
    *)
    systemctl enable juno-pp.service
    ;;
esac

#Rules for ACPI
case "$product_model" in
    NL50RU|NL51RU|NL5xRU|NL4x_NL5xRU|NL5xNU|NL4x_NLxNU|NH5xVR)
    if [ -f /etc/udev/rules.d/juno-cpufreq.rules ]; then
    rm /etc/udev/rules.d/juno-cpufreq.rules;
    else
    ln -s /usr/share/nv41/udev/juno-cpufreq.rules /etc/udev/rules.d/juno-cpufreq.rules
    fi
    ;;
esac

#FIX AUDIO
case "$product_model" in
    PE60RNE_RND_RNC)
    if [ -f /etc/modprobe.d/juno-audio-fix.conf ]; then
       rm /etc/modprobe.d/juno-audio-fix.conf;
       cp /usr/share/junocomp/juno-audio-fix.conf /etc/modprobe.d;
       else
       cp /usr/share/junocomp/juno-audio-fix.conf /etc/modprobe.d;
    fi
    ;;
esac

#Fix i2c_hid after suspend
case "$product_model" in
    NP5x_NP6x_NP7xHP|NP5x_NP7xHH_HJ_HK)
    if [ -f /usr/lib/systemd/system-sleep/restore-i2c-hid ]; then
       rm /usr/lib/systemd/system-sleep/restore-i2c-hid;
       cp /usr/share/junocomp/restore-i2c-hid /usr/lib/systemd/system-sleep/;
       chmod +x /usr/lib/systemd/system-sleep/restore-i2c-hid;
       else
       cp /usr/share/junocomp/restore-i2c-hid /usr/lib/systemd/system-sleep/;
       chmod +x /usr/lib/systemd/system-sleep/restore-i2c-hid;
    fi
    ;;
esac

#MSR Module
if grep -q 'msr' "/etc/modules";
   then
     echo 'msr module exist';
   else
     echo "Adding msr to modules"
     echo msr >> /etc/modules;
     modprobe msr;
fi

#Enable LM92
if ! grep -Fxq 'lm92' '/etc/modules' ; then
   echo 'Adding lm92';
   echo 'lm92' >> /etc/modules;
   modprobe lm92;

   else echo 'lm92 already enabled';
fi

#Enable ec_sys
if ! grep -Fxq 'ec_sys' '/etc/modules' ; then
   echo 'Adding ec_sys';
   echo 'ec_sys' >> /etc/modules;
   modprobe ec_sys;

   else echo 'ec_sys already in /etc/modules';
fi

# Install Brasero icons for Juno Fan Indicator
cp /usr/share/brasero/icons/hicolor/scalable/status/brasero-disc-* /usr/share/icons/hicolor/scalable/status/

# Update Schemas
glib-compile-schemas /usr/share/glib-2.0/schemas/